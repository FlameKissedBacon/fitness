package model;

import java.time.LocalDate;

public abstract class Exercise {
	protected LocalDate exerciseDate;
	protected String exerciseName;
	protected int exerciseSeconds;
	protected int rowID;
	protected int personID;

	public int getPersonID() {
		return personID;
	}

	public void setPersonID(int personID) {
		this.personID = personID;
	}

	public int getRowID() {
		return rowID;
	}

	public void setRowID(int rowID) {
		this.rowID = rowID;
	}

	public LocalDate getExerciseDate() {
		return exerciseDate;
	}

	public void setExerciseDate(LocalDate exerciseDate) {
		this.exerciseDate = exerciseDate;
	}

	public String getExerciseName() {
		return exerciseName;
	}

	public void setExerciseName(String exerciseName) {
		this.exerciseName = exerciseName;
	}

	public int getExerciseSeconds() {
		return exerciseSeconds;
	}

	public void setExerciseSeconds(int exerciseSeconds) {
		this.exerciseSeconds = exerciseSeconds;
	}
	
	public abstract void save();
		
	public abstract void delete();
	
	

}
