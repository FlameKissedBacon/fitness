package model;

import java.util.ArrayList;
import java.util.List;

import db.Database;
import db.Parameter;

public class ExerciseStrength extends Exercise {
	private int sets;
	private int reps;
	public int weightLifted;

	public int getSets() {
		return sets;
	}

	public void setSets(int sets) {
		this.sets = sets;
	}

	public int getReps() {
		return reps;
	}

	public void setReps(int reps) {
		this.reps = reps;
	}

	public int getWeightLifted() {
		return weightLifted;
	}

	public void setWeightLifted(int weightLifted) {
		this.weightLifted = weightLifted;
	}

	@Override
	public void save() {
		Database db = new Database("www.berkstresser.org", "Exercise");
		List<Parameter> params = new ArrayList<>();
		
		params.add(new Parameter(personID));
		params.add(new Parameter(exerciseDate));
		params.add(new Parameter(exerciseName));
		params.add(new Parameter(exerciseSeconds));
		params.add(new Parameter(sets));
		params.add(new Parameter(reps));
		params.add(new Parameter(weightLifted));
		

		db.executeSql("usp_CreateExerciseStrength", params);

	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub

	}

}
