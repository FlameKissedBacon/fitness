package model;

public class Conversion {
	
	private Double feet;
	private Double meters;
	private Double inches;
	private Double pounds;
	private Double kg;
	
	public Double getInches() {
		return inches;
	}

	public void setInches(Double inches) {
		this.inches = inches;
	}

	public Double getPounds() {
		return pounds;
	}

	public void setPounds(Double pounds) {
		this.pounds = pounds;
	}

	public Double getKg() {
		return kg;
	}

	public void setKg(Double kg) {
		this.kg = kg;
	}
	
	public Double getMeters() {
		return meters;
	}

	public void setMeters(Double meters) {
		this.meters = meters;
	}

	public Double getFeet() {
		return feet;
	}

	public void setFeet(Double feet) {
		this.feet = feet;
	}

	public Double getFeetToMeters() {
		return (feet * 0.3048);
	}
	
	public Double getPoundsToKg( ) {
		return (pounds * 0.453592);
	}
	
	public  Double getFeetToInches() {
		inches = (feet * 12) + inches;
		return inches;
	}
	
	public Double getMetersToFeet() {
		return meters / 0.3048;
	}


}
