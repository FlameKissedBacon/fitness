package model;

import java.util.ArrayList;
import java.util.List;

import db.Database;
import db.Parameter;

public class ExerciseAerobic extends Exercise {
	private int maximumHeartRate;
	private int averageHeartRate;
	
	public int getCaloriedBurned() {
		// TODO: figure out how to get the formula
		return 0;
	}

	public int getMaximumHeartRate() {
		return maximumHeartRate;
	}

	public void setMaximumHeartRate(int maximumHeartRate) {
		this.maximumHeartRate = maximumHeartRate;
	}

	public int getAverageHeartRate() {
		return averageHeartRate;
	}

	public void setAverageHeartRate(int averageHeartRate) {
		this.averageHeartRate = averageHeartRate;
	}

	@Override
	public void save() {
		Database db = new Database("www.berkstresser.org", "Exercise");
		List<Parameter> params = new ArrayList<>();
		
		params.add(new Parameter(personID));
		params.add(new Parameter(exerciseDate));
		params.add(new Parameter(exerciseName));
		params.add(new Parameter(exerciseSeconds));
		params.add(new Parameter(maximumHeartRate));
		params.add(new Parameter(averageHeartRate));
		

		db.executeSql("usp_CreateExerciseAerobic", params);
		
	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
	}

}
