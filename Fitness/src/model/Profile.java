package model;

import java.security.InvalidParameterException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import db.Database;
import db.Parameter;
import gui.NewProfileGui;

/**
 * public class.
 * 
 * @author Michael
 *
 */
public class Profile {

	/**
	 * the weight in kilos.
	 */
	private double weight; // kilograms

	/**
	 * the height in meters.
	 */
	private double height; // meters

	/**
	 * The string Sex.
	 */
	private String sex;

	/**
	 * the LocalDate birthday.
	 */
	private LocalDate birthday;

	/**
	 * the string firstname.
	 */
	private String firstName = "Default";

	/**
	 * the string lastname.
	 */
	private String lastName = "Default";

	/**
	 * the string studentID.
	 */
	private String studentID;

	/**
	 * the string password.
	 */
	private String password;

	/**
	 * The arraylist of aerobic exercises.
	 */
	private List<ExerciseAerobic> aerobics = new ArrayList<>();

	/**
	 * return aerobics list.
	 * 
	 * @return aerobics
	 */
	public List<ExerciseAerobic> getAerobics() {
		return aerobics;
	}

	/**
	 * Ill be honest not too sure what this does.
	 * 
	 * @param aerobics
	 */
	public void setAerobics(final List<ExerciseAerobic> aerobics) {
		this.aerobics = aerobics;
	}

	/**
	 * Ill be honest not too sure what this does.
	 * 
	 * @param positionToGet
	 * @return aerobics
	 */
	public ExerciseAerobic getAerobic(final int positionToGet) {
		return aerobics.get(positionToGet);
	}

	/**
	 * Ill be honest not too sure what this does.
	 * 
	 * @param exerciseToAdd
	 */
	public void addAerobic(final ExerciseAerobic exerciseToAdd) {
		aerobics.add(exerciseToAdd);
	}

	/**
	 * Ill be honest not too sure what this does.
	 * 
	 * @param positionToRemove
	 */
	public void removeAerobic(final int positionToRemove) {
		aerobics.remove(positionToRemove);
	}
	
	private List<ExerciseStrength> strength = new ArrayList<>();

	/**
	 * return aerobics list.
	 * 
	 * @return aerobics
	 */
	public List<ExerciseStrength> getStrength() {
		return strength;
	}

	/**
	 * Ill be honest not too sure what this does.
	 * 
	 * @param aerobics
	 */
	public void setStrength(final List<ExerciseStrength> strength) {
		this.strength = strength;
	}

	/**
	 * Ill be honest not too sure what this does.
	 * 
	 * @param positionToGet
	 * @return aerobics
	 */
	public ExerciseStrength getStrength(final int positionToGet) {
		return strength.get(positionToGet);
	}

	/**
	 * Ill be honest not too sure what this does.
	 * 
	 * @param exerciseToAdd
	 */
	public void addStrength(final ExerciseStrength exerciseToAdd) {
		strength.add(exerciseToAdd);
	}

	/**
	 * Ill be honest not too sure what this does.
	 * 
	 * @param positionToRemove
	 */
	public void removeStrength(final int positionToRemove) {
		strength.remove(positionToRemove);
	}

	/**
	 * creates the public profile.
	 */
	public Profile() {

	}

	/**
	 * returns the profile info based on the student ID and pass.
	 * 
	 * @param pStudentID
	 * @param pPassword
	 */
	public Profile(final String pStudentID, final String pPassword) {
		Database db = new Database("www.berkstresser.org", "Exercise");
		List<Parameter> params = new ArrayList<>();

		params.add(new Parameter<String>(pStudentID));
		params.add(new Parameter<String>(pPassword));

		ResultSet profile = db.getResultSet("usp_GetProfileByStudentIDAndPassword", params);

		try {
			if (profile.next()) {
				setBirthday(birthday = profile.getDate("Birthday").toLocalDate());
				setFirstName(firstName = profile.getString("FirstName"));
				setLastName(lastName = profile.getString("LastName"));
				setHeightInInches(height = profile.getDouble("Height"));
				setWeightInPounds(weight = profile.getDouble("Weight"));
				setPassword(password = profile.getString("Password"));
				setSex(sex = profile.getString("Sex"));
				setStudentID(studentID = profile.getString("StudentID"));
			} else {
				throw new InvalidParameterException("Credentials Invalid");

			}

			// now load aerobic exercises
			params = new ArrayList<>();
			params.add(new Parameter<String>(pStudentID));
			ResultSet exercises = db.getResultSet("usp_GetExercisesAerobicByPerson", params);
			while (exercises.next()) {
				ExerciseAerobic e = new ExerciseAerobic();
				e.setRowID(exercises.getInt("RowID"));
				e.setExerciseDate(exercises.getDate("ExerciseDate").toLocalDate());
				e.setExerciseName(exercises.getString("ExerciseName"));
				e.setMaximumHeartRate(exercises.getInt("MaximumHeartRate"));
				e.setAverageHeartRate(exercises.getInt("AverageHeartRate"));

				aerobics.add(e);
			}
			
			// now load strength exercises
			params = new ArrayList<>();
			params.add(new Parameter<String>(pStudentID));
			ResultSet sExercises = db.getResultSet("usp_GetExercisesStrengthByPerson", params);
			while (sExercises.next()) {
				ExerciseStrength e = new ExerciseStrength();
				e.setRowID(sExercises.getInt("RowID"));
				e.setExerciseDate(sExercises.getDate("ExerciseDate").toLocalDate());
				e.setExerciseName(sExercises.getString("ExerciseName"));
				e.setSets(sExercises.getInt("sets"));
				e.setReps(sExercises.getInt("reps"));
				e.setReps(sExercises.getInt("reps"));
				
				strength.add(e);
			}

		} catch (SQLException e) {
			// e.printStackTrace();
		}

	}

	/**
	 * @return the age
	 */
	public int getAge() {
		try {
			Period age = Period.between(birthday, LocalDate.now());
			return age.getYears();
		} catch (NullPointerException n) {
			return 0;
		}
	}

	/**
	 * @return the weight
	 */
	public final double getWeightInKilograms() {
		return weight;
	}

	/**
	 * 
	 * @return the weight in pounds
	 */
	public double getWeightInPounds() {
		return weight;
	}

	/**
	 * @return the height
	 */
	public final double getHeightInMeters() {
		return height;
	}

	/**
	 * 
	 * @return the height in inches
	 */
	public double getHeightInInches() {
		return height;
	}

	/**
	 * @return the sex
	 */
	public final String getSex() {
		return sex;
	}

	/**
	 * @return the birthday
	 */
	public final LocalDate getBirthday() {
		return birthday;
	}

	/**
	 * @return the firstName
	 */
	public final String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public final String getLastName() {
		return lastName;
	}

	/**
	 * @return the studentID
	 */
	public final String getStudentID() {
		return studentID;
	}

	/**
	 * @return the password
	 */
	public final String getPassword() {
		return password;
	}

	/**
	 * @param pWeight
	 *            the weight to set
	 */
	public final void setWeightInKilograms(final double pWeight) {
		weight = pWeight;
	}

	/**
	 * 
	 * @param pWeight
	 *            the weight to set
	 */
	public void setWeightInPounds(final double pWeight) {
		weight = pWeight;
	}

	/**
	 * @param pHeight
	 *            the height to set
	 */
	public final void setHeightInMeters(final double pHeight) {
		height = pHeight;
	}

	/**
	 * 
	 * @param pHeight
	 *            the height to set
	 */
	public void setHeightInInches(final double pHeight) {
		height = pHeight;
	}

	/**
	 * @param string
	 *            the sex to set
	 */
	public final void setSex(final String string) {
		sex = string;
	}

	/**
	 * @param pBirthday
	 *            the birthday to set
	 */
	public final void setBirthday(final LocalDate pBirthday) {
		birthday = pBirthday;
	}

	/**
	 * @param pFirstName
	 *            the firstName to set
	 */
	public final void setFirstName(final String pFirstName) {
		firstName = pFirstName;
	}

	/**
	 * @param pLastName
	 *            the lastName to set
	 */
	public final void setLastName(final String pLastName) {
		lastName = pLastName;
	}

	/**
	 * @param pStudentID
	 *            the studentID to set
	 */
	public final void setStudentID(final String pStudentID) {
		studentID = pStudentID;
	}

	/**
	 * @param pPassword
	 *            the password to set
	 */
	public final void setPassword(final String pPassword) {
		password = pPassword;
	}

	/**
	 * save the profile info to the database.
	 * @throws Exception 
	 */
	public void save() throws Exception {
		try {
		Database db = new Database("www.berkstresser.org", "Exercise");
		List<Parameter> params = new ArrayList<>();

		params.add(new Parameter(firstName));
		params.add(new Parameter(lastName));
		params.add(new Parameter(height));
		params.add(new Parameter(weight));
		params.add(new Parameter(sex));
		params.add(new Parameter(birthday));
		params.add(new Parameter(studentID));
		params.add(new Parameter(password));

		db.executeSql("usp_CreateProfile", params);
		
		// FIXME: duplicate entry
		} catch (Exception e) {
			throw new Exception(e);
		}
		
	}

	/**
	 * update the profile information.
	 */
	public void update() {
		Database db = new Database("www.berkstresser.org", "Exercise");
		List<Parameter> params = new ArrayList<>();

		params.add(new Parameter(firstName));
		params.add(new Parameter(lastName));
		params.add(new Parameter(height));
		params.add(new Parameter(weight));
		params.add(new Parameter(sex));
		params.add(new Parameter(birthday));
		params.add(new Parameter(studentID));
		params.add(new Parameter(password));

		db.executeSql("usp_UpdateProfile", params);

	}

}
