package model;

import java.awt.event.KeyEvent;

import javax.swing.JTextField;

/**
 * Public class.
 * @author Michael
 *
 */
public class Tools {
	//TODO: test the keylisteners to see if they work.

	/**
	 * Checks to see if a value is empty.
	 * @param value 
	 * @return true false
	 */
	public boolean isEmpty(final String value) {
		return value.equals("");
	}

	/**
	 * A keylistener to allow numbers and one decimal.
	 * @param e 
	 */
	public void numbersOneDecimal(final KeyEvent e) {
		// if any key from 0 to 9 are pressed, consume them
		if (e.getKeyChar() < KeyEvent.VK_0 && e.getKeyChar() > KeyEvent.VK_9) {
			// if decimal is pressed, allow it
			if (e.getKeyChar() != KeyEvent.VK_PERIOD) {
				e.consume();
			} // end if
				// if box already holds a decimal, consume any extra decimals
				// this is away from the letters
			if (e.getKeyChar() == KeyEvent.VK_PERIOD && ((JTextField) e.getSource()).getText().contains(".")) {
				e.consume();
			} // end if
		} // end if
	}

	/**
	 * A key listener to allow numbers with no decimal input.
	 * @param e 
	 */
	public void numbersOnly(final KeyEvent e) {
		// if any key besides 0 to 9 are pressed, consume them
		if (e.getKeyChar() >= KeyEvent.VK_0 && e.getKeyChar() <= KeyEvent.VK_9) {
			// let it through
		} else {
			e.consume();
		} // end else
	} // end listener

	/**
	 * A key Listener to allow only letter input.
	 * @param e 
	 */
	public void lettersOnly(final KeyEvent e) {
		// if any key from 0 to 9 are pressed, consume them
		if (e.getKeyChar() < KeyEvent.VK_A || e.getKeyChar() > KeyEvent.VK_Z) {
			// let it through
		} else {
			e.consume();
		} // end else
	} // end listener

	/**
	 * A keylistener to allow letters numbers and one decimal as input.
	 * @param e 
	 */
	public void lettersNumbersOneDecimal(final KeyEvent e) {
		if (e.getKeyChar() < KeyEvent.VK_A || e.getKeyChar() > KeyEvent.VK_Z) {
			// let it through
			// if any key from 0 to 9 are pressed, consume them
			if (e.getKeyChar() > KeyEvent.VK_0 || e.getKeyChar() < KeyEvent.VK_9) {
				// if decimal is pressed, allow it
				if (e.getKeyChar() != KeyEvent.VK_PERIOD) {
					e.consume();
				} // end if
					// if box already holds a decimal, consume any extra
					// decimals
					// this is away from the letters
				if (e.getKeyChar() == KeyEvent.VK_PERIOD && ((JTextField) e.getSource()).getText().contains(".")) {
					e.consume();
				} // end if
			} // end listener
		}
	}

	/**
	 * A keylistener to allow letters and numbers only.
	 * @param e 
	 */
	public void lettersNumbersOnly(final KeyEvent e) {
		if (e.getKeyChar() < KeyEvent.VK_A || e.getKeyChar() > KeyEvent.VK_Z) {
			// if any key from 0 to 9 are pressed, consume them
			if (e.getKeyChar() > KeyEvent.VK_0 || e.getKeyChar() < KeyEvent.VK_9) {
				//do nothing
			} else {
				e.consume();
			}
		}
	}

}
