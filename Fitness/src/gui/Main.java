package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.security.InvalidParameterException;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import model.Conversion;
import model.ExerciseAerobic;
import model.ExerciseStrength;
import model.Profile;
import model.Tools;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JList;

public class Main {

	JFrame frmFitness;
	private JTextField txtStuID;
	private JTextField txtFirstName;
	private JTextField txtLastName;
	private JTextField txtAge;
	private JTextField txtWeight;
	private JTextField txtHeight;
	private JTextField txtCurrentFirstName;
	private JTextField txtCurrentLastName;
	private JTextField txtCurrentAge;
	private JTextField txtCurrentWeight;
	private JTextField txtCurrentHeight;
	private JButton btnLogIn;
	private JPanel pnlProfile;
	private JPanel pnlDataCollection;
	private JPanel pnlSuggested;
	private JTabbedPane tabbedPane;
	private JButton btnUpdate;
	private JPasswordField txtPassword;
	private JButton btnSkipLogin;
	private JPanel pnlFootToMeter;
	private JLabel lblFeet;
	private JTextField txtFeetConvert;
	private JButton btnConvertF2M;
	private JLabel lblCurrentSex;
	private JTextField txtCurrentSex;
	private JButton btnCreateAccount;
	private String[] SEX = { "Select your Sex", "Male", "Female", "Other" };

	Tools t = new Tools();
	Profile myProfile = new Profile();
	DecimalFormat df = new DecimalFormat("##.##");
	ExerciseAerobic ea = new ExerciseAerobic();
	ExerciseStrength es = new ExerciseStrength();

	private JLabel lblSex;
	private JComboBox<String> cboSex;
	private JPanel pnlMeterToFoot;
	private JLabel lblLbs;
	private JTextField txtConvertToKg;
	private JButton btnConvertL2K;
	private JPanel panel_1;
	private JLabel lblMaximumHeartrate;
	private JTextField txtMaxHeartrate;
	private JLabel lblAverageHeartrate;
	private JTextField txtAverageHeartrate;
	private JButton btnAddExercise;
	private JPanel panel_2;
	private JLabel lblReps;
	private JTextField txtReps;
	private JLabel lblSets;
	private JTextField txtSets;
	private JLabel lblWeightsLifted;
	private JTextField txtWeightsLifted;
	private JPanel panel_3;
	private JLabel lblExerciseDate;
	private JTextField txtExerciseDate;
	private JLabel lblExerciseName;
	private JTextField txtExerciseName;
	private JLabel lblExerciseTimesec;
	private JTextField txtExerciseTime;
	private JLabel lblPasswordUpdate;
	private JTextField txtPassUpdate;
	private JList listSummary;
	private JButton btnUpdate_1;

	public void Profile() {

	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frmFitness.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmFitness = new JFrame("Fitness");
		frmFitness.setBounds(100, 100, 506, 388);
		frmFitness.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frmFitness.getContentPane().setLayout(springLayout);

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addFocusListener(new TabbedPaneFocusListener());
		springLayout.putConstraint(SpringLayout.NORTH, tabbedPane, 0, SpringLayout.NORTH, frmFitness.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, tabbedPane, 0, SpringLayout.WEST, frmFitness.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, tabbedPane, 0, SpringLayout.SOUTH, frmFitness.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, tabbedPane, 0, SpringLayout.EAST, frmFitness.getContentPane());
		frmFitness.getContentPane().add(tabbedPane);

		JPanel pnlWelcome = new JPanel();
		tabbedPane.addTab("Login", null, pnlWelcome, null);
		SpringLayout sl_pnlWelcome = new SpringLayout();
		pnlWelcome.setLayout(sl_pnlWelcome);

		JPanel panel = new JPanel();
		sl_pnlWelcome.putConstraint(SpringLayout.NORTH, panel, 65, SpringLayout.NORTH, pnlWelcome);
		sl_pnlWelcome.putConstraint(SpringLayout.WEST, panel, 60, SpringLayout.WEST, pnlWelcome);
		sl_pnlWelcome.putConstraint(SpringLayout.SOUTH, panel, -71, SpringLayout.SOUTH, pnlWelcome);
		sl_pnlWelcome.putConstraint(SpringLayout.EAST, panel, -57, SpringLayout.EAST, pnlWelcome);
		panel.setBorder(new TitledBorder(null, "Login", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlWelcome.add(panel);

		JLabel lblWelcomeToThe = new JLabel(
				"<html>Welcome to the Quit This Fitness Application!<br /><center>Please Log in.</html>");
		sl_pnlWelcome.putConstraint(SpringLayout.NORTH, lblWelcomeToThe, 10, SpringLayout.NORTH, pnlWelcome);
		sl_pnlWelcome.putConstraint(SpringLayout.SOUTH, lblWelcomeToThe, -6, SpringLayout.NORTH, panel);
		sl_pnlWelcome.putConstraint(SpringLayout.EAST, lblWelcomeToThe, -105, SpringLayout.EAST, pnlWelcome);
		panel.setLayout(null);

		JLabel lblName = new JLabel("Student ID");
		lblName.setBounds(18, 24, 108, 14);
		panel.add(lblName);

		txtStuID = new JTextField();
		txtStuID.addKeyListener(new KeyStopperNumbersOneDecimal());
		txtStuID.setBounds(165, 21, 179, 20);
		panel.add(txtStuID);
		txtStuID.setColumns(10);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(18, 49, 108, 14);
		panel.add(lblPassword);

		btnLogIn = new JButton("Log in");
		btnLogIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				login();
			}
		});

		btnLogIn.setBounds(28, 79, 137, 23);
		panel.add(btnLogIn);

		txtPassword = new JPasswordField();
		txtPassword.setBounds(165, 46, 179, 19);
		panel.add(txtPassword);
		pnlWelcome.add(lblWelcomeToThe);

		btnCreateAccount = new JButton("Create Account");
		btnCreateAccount.addActionListener(new BtnCreateAccountActionListener());
		btnCreateAccount.setBounds(194, 79, 150, 23);
		panel.add(btnCreateAccount);

		btnSkipLogin = new JButton("Skip Login");
		btnSkipLogin.setBounds(121, 128, 115, 23);
		panel.add(btnSkipLogin);
		btnSkipLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tabbedPane.setEnabledAt(1, true);
				tabbedPane.setEnabledAt(2, true);
				tabbedPane.setEnabledAt(3, true);
			}
		});

		sl_pnlWelcome.putConstraint(SpringLayout.NORTH, btnSkipLogin, 6, SpringLayout.SOUTH, panel);
		sl_pnlWelcome.putConstraint(SpringLayout.EAST, btnSkipLogin, -165, SpringLayout.EAST, pnlWelcome);

		pnlProfile = new JPanel();
		tabbedPane.addTab("Personal Information", null, pnlProfile, null);
		tabbedPane.setEnabledAt(1, false);
		SpringLayout sl_pnlProfile = new SpringLayout();
		pnlProfile.setLayout(sl_pnlProfile);

		JPanel pnlUpdate = new JPanel();
		sl_pnlProfile.putConstraint(SpringLayout.NORTH, pnlUpdate, 10, SpringLayout.NORTH, pnlProfile);
		sl_pnlProfile.putConstraint(SpringLayout.WEST, pnlUpdate, 10, SpringLayout.WEST, pnlProfile);
		sl_pnlProfile.putConstraint(SpringLayout.SOUTH, pnlUpdate, -10, SpringLayout.SOUTH, pnlProfile);
		sl_pnlProfile.putConstraint(SpringLayout.EAST, pnlUpdate, -225, SpringLayout.EAST, pnlProfile);
		pnlUpdate.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Personal Information Update",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlProfile.add(pnlUpdate);
		pnlUpdate.setLayout(new GridLayout(0, 2, 0, 0));

		JLabel lblFirstName = new JLabel("First Name");
		pnlUpdate.add(lblFirstName);

		// letters only
		txtFirstName = new JTextField();
		txtFirstName.addKeyListener(new KeyStopperLettersOnly());
		pnlUpdate.add(txtFirstName);
		txtFirstName.setColumns(10);

		JLabel lblLastName = new JLabel("Last Name");
		pnlUpdate.add(lblLastName);

		// letters only
		txtLastName = new JTextField();
		txtLastName.addKeyListener(new KeyStopperLettersOnly());
		pnlUpdate.add(txtLastName);
		txtLastName.setColumns(10);

		JLabel lblAge = new JLabel("Birthday");
		pnlUpdate.add(lblAge);

		// numbers only no decimal
		txtAge = new JTextField();
		txtAge.addKeyListener(new KeyStopperNumbersOnly());
		txtAge.addFocusListener(new TxtAgeFocusListener());
		txtAge.setText("MMDDYYYY");
		pnlUpdate.add(txtAge);
		txtAge.setColumns(10);

		JLabel lblWeight = new JLabel("Weight (Kg)");
		pnlUpdate.add(lblWeight);

		// numbers only 1 decimal
		txtWeight = new JTextField();
		txtWeight.addKeyListener(new KeyStopperNumbersOneDecimal());
		pnlUpdate.add(txtWeight);
		txtWeight.setColumns(10);

		JLabel lblHeight = new JLabel("Height (Meters)");
		pnlUpdate.add(lblHeight);

		btnUpdate = new JButton("Update Profile");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				updateProfile();
			}
		});

		txtHeight = new JTextField();
		txtHeight.addKeyListener(new KeyStopperNumbersOneDecimal());
		pnlUpdate.add(txtHeight);
		txtHeight.setColumns(10);

		cboSex = new JComboBox<String>();
		cboSex.setModel(new DefaultComboBoxModel<>(SEX));

		lblSex = new JLabel("Sex");
		pnlUpdate.add(lblSex);
		pnlUpdate.add(cboSex);

		lblPasswordUpdate = new JLabel("Password");
		pnlUpdate.add(lblPasswordUpdate);

		txtPassUpdate = new JTextField();
		pnlUpdate.add(txtPassUpdate);
		txtPassUpdate.setColumns(10);
		pnlUpdate.add(btnUpdate);

		JPanel pnlCurrent = new JPanel();
		sl_pnlProfile.putConstraint(SpringLayout.NORTH, pnlCurrent, 10, SpringLayout.NORTH, pnlProfile);
		sl_pnlProfile.putConstraint(SpringLayout.WEST, pnlCurrent, 6, SpringLayout.EAST, pnlUpdate);
		sl_pnlProfile.putConstraint(SpringLayout.SOUTH, pnlCurrent, -138, SpringLayout.SOUTH, pnlProfile);
		sl_pnlProfile.putConstraint(SpringLayout.EAST, pnlCurrent, -10, SpringLayout.EAST, pnlProfile);
		pnlCurrent.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"Current Personal Information", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlProfile.add(pnlCurrent);
		pnlCurrent.setLayout(new GridLayout(0, 2, 0, 0));

		lblCurrentSex = new JLabel("Sex");
		pnlCurrent.add(lblCurrentSex);

		txtCurrentSex = new JTextField();
		txtCurrentSex.setEditable(false);
		pnlCurrent.add(txtCurrentSex);
		txtCurrentSex.setColumns(10);

		JLabel lblCurrentFirstName = new JLabel("First Name");
		pnlCurrent.add(lblCurrentFirstName);

		txtCurrentFirstName = new JTextField();
		txtCurrentFirstName.setEditable(false);
		txtCurrentFirstName.setColumns(10);
		pnlCurrent.add(txtCurrentFirstName);

		JLabel lblCurrentLastName = new JLabel("Last Name");
		pnlCurrent.add(lblCurrentLastName);

		txtCurrentLastName = new JTextField();
		txtCurrentLastName.setEditable(false);
		txtCurrentLastName.setColumns(10);
		pnlCurrent.add(txtCurrentLastName);

		JLabel lblCurrentAge = new JLabel("Age");
		pnlCurrent.add(lblCurrentAge);

		txtCurrentAge = new JTextField();
		txtCurrentAge.setEditable(false);
		txtCurrentAge.setColumns(10);
		pnlCurrent.add(txtCurrentAge);

		JLabel lblCurrentWeight = new JLabel("Weight");
		pnlCurrent.add(lblCurrentWeight);

		txtCurrentWeight = new JTextField();
		txtCurrentWeight.setEditable(false);
		txtCurrentWeight.setColumns(10);
		pnlCurrent.add(txtCurrentWeight);

		JLabel lblCurrentHeight = new JLabel("Height");
		pnlCurrent.add(lblCurrentHeight);

		txtCurrentHeight = new JTextField();
		txtCurrentHeight.setEditable(false);
		txtCurrentHeight.setColumns(10);
		pnlCurrent.add(txtCurrentHeight);

		pnlFootToMeter = new JPanel();
		sl_pnlProfile.putConstraint(SpringLayout.NORTH, pnlFootToMeter, 6, SpringLayout.SOUTH, pnlCurrent);
		sl_pnlProfile.putConstraint(SpringLayout.WEST, pnlFootToMeter, 6, SpringLayout.EAST, pnlUpdate);
		sl_pnlProfile.putConstraint(SpringLayout.SOUTH, pnlFootToMeter, 0, SpringLayout.SOUTH, pnlUpdate);
		sl_pnlProfile.putConstraint(SpringLayout.EAST, pnlFootToMeter, -104, SpringLayout.EAST, pnlCurrent);
		pnlFootToMeter.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Feet to Meters",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlProfile.add(pnlFootToMeter);
		pnlFootToMeter.setLayout(new GridLayout(0, 1, 0, 0));

		lblFeet = new JLabel("Feet ");
		pnlFootToMeter.add(lblFeet);

		txtFeetConvert = new JTextField();
		txtFeetConvert.addKeyListener(new KeyStopperNumbersOneDecimal());
		pnlFootToMeter.add(txtFeetConvert);
		txtFeetConvert.setColumns(10);

		btnConvertF2M = new JButton("Convert");
		btnConvertF2M.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Conversion convert = new Conversion();
				convert.setFeet(Double.parseDouble(txtFeetConvert.getText()));
				txtHeight.setText(String.valueOf(df.format(convert.getFeetToMeters())));
			}
		});

		pnlFootToMeter.add(btnConvertF2M);

		pnlMeterToFoot = new JPanel();
		sl_pnlProfile.putConstraint(SpringLayout.NORTH, pnlMeterToFoot, 6, SpringLayout.SOUTH, pnlCurrent);
		sl_pnlProfile.putConstraint(SpringLayout.WEST, pnlMeterToFoot, 6, SpringLayout.EAST, pnlFootToMeter);
		sl_pnlProfile.putConstraint(SpringLayout.SOUTH, pnlMeterToFoot, 0, SpringLayout.SOUTH, pnlUpdate);
		sl_pnlProfile.putConstraint(SpringLayout.EAST, pnlMeterToFoot, -6, SpringLayout.EAST, pnlProfile);
		pnlMeterToFoot.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Lbs to Kg",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlProfile.add(pnlMeterToFoot);
		pnlMeterToFoot.setLayout(new GridLayout(0, 1, 0, 0));

		lblLbs = new JLabel("Pounds");
		pnlMeterToFoot.add(lblLbs);

		txtConvertToKg = new JTextField();
		txtConvertToKg.addKeyListener(new KeyStopperNumbersOneDecimal());
		txtConvertToKg.setColumns(10);
		pnlMeterToFoot.add(txtConvertToKg);

		btnConvertL2K = new JButton("Convert");
		btnConvertL2K.addActionListener(new BtnConvertM2FActionListener());
		pnlMeterToFoot.add(btnConvertL2K);

		pnlDataCollection = new JPanel();
		tabbedPane.addTab("Exercise Recording", null, pnlDataCollection, null);
		tabbedPane.setEnabledAt(2, false);
		pnlDataCollection.setLayout(null);

		panel_1 = new JPanel();
		panel_1.setBounds(10, 103, 220, 84);
		panel_1.setBorder(new TitledBorder(null, "Aerobics", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlDataCollection.add(panel_1);
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));

		lblMaximumHeartrate = new JLabel("Max Heartrate");
		panel_1.add(lblMaximumHeartrate);

		// numbers only
		txtMaxHeartrate = new JTextField();
		txtMaxHeartrate.addKeyListener(new KeyStopperNumbersOnly());
		panel_1.add(txtMaxHeartrate);
		txtMaxHeartrate.setColumns(10);

		lblAverageHeartrate = new JLabel("Ave. Heartrate");
		panel_1.add(lblAverageHeartrate);

		// numbers only
		txtAverageHeartrate = new JTextField();
		txtAverageHeartrate.addKeyListener(new KeyStopperNumbersOnly());
		panel_1.add(txtAverageHeartrate);
		txtAverageHeartrate.setColumns(10);

		btnAddExercise = new JButton("Add Exercise");
		btnAddExercise.setBounds(238, 12, 109, 23);
		btnAddExercise.addActionListener(new BtnAddExerciseActionListener());
		pnlDataCollection.add(btnAddExercise);

		panel_2 = new JPanel();
		panel_2.setBounds(10, 198, 220, 113);
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Strength", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlDataCollection.add(panel_2);

		lblReps = new JLabel("Reps");

		// numbers only
		txtReps = new JTextField();
		txtReps.addKeyListener(new KeyStopperNumbersOnly());
		txtReps.setColumns(10);

		// numbers only
		txtSets = new JTextField();
		txtSets.addKeyListener(new KeyStopperNumbersOnly());
		txtSets.setColumns(10);

		txtWeightsLifted = new JTextField();
		txtWeightsLifted.addKeyListener(new KeyStopperNumbersOneDecimal());
		txtWeightsLifted.setColumns(10);
		panel_2.setLayout(new GridLayout(0, 2, 0, 0));
		panel_2.add(lblReps);
		panel_2.add(txtReps);

		lblSets = new JLabel("Sets");
		panel_2.add(lblSets);
		panel_2.add(txtSets);

		lblWeightsLifted = new JLabel("Weights Lifted");
		panel_2.add(lblWeightsLifted);
		panel_2.add(txtWeightsLifted);

		panel_3 = new JPanel();
		panel_3.setBounds(10, 10, 220, 82);
		panel_3.setBorder(
				new TitledBorder(null, "Exercise General Info", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlDataCollection.add(panel_3);
		panel_3.setLayout(new GridLayout(0, 2, 0, 0));

		lblExerciseDate = new JLabel("Exercise Date");
		panel_3.add(lblExerciseDate);

		// numbers only
		txtExerciseDate = new JTextField();
		txtExerciseDate.addKeyListener(new KeyStopperNumbersOnly());

		panel_3.add(txtExerciseDate);
		txtExerciseDate.setColumns(10);

		lblExerciseName = new JLabel("Exercise Name");
		panel_3.add(lblExerciseName);

		// letters only
		txtExerciseName = new JTextField();
		txtExerciseName.addKeyListener(new KeyStopperLettersOnly());
		panel_3.add(txtExerciseName);
		txtExerciseName.setColumns(10);

		lblExerciseTimesec = new JLabel("Time (Sec.)");
		panel_3.add(lblExerciseTimesec);

		// numbers only
		txtExerciseTime = new JTextField();
		txtExerciseTime.addKeyListener(new KeyStopperNumbersOnly());
		panel_3.add(txtExerciseTime);
		txtExerciseTime.setColumns(10);

		JButton btnLoadSelected = new JButton("Load Selected");
		btnLoadSelected.setBounds(366, 12, 109, 23);
		btnLoadSelected.addActionListener(new BtnLoadSelectedActionListener());
		pnlDataCollection.add(btnLoadSelected);

		listSummary = new JList();
		listSummary.setSelectedIndex(1);
		listSummary.setBounds(238, 73, 237, 238);
		pnlDataCollection.add(listSummary);
		updateExercises();

		btnUpdate_1 = new JButton("Update");
		btnUpdate_1.addActionListener(new BtnUpdate_1ActionListener());
		btnUpdate_1.setBounds(315, 39, 89, 23);
		pnlDataCollection.add(btnUpdate_1);

		pnlSuggested = new JPanel();
		tabbedPane.addTab("Do This!", null, pnlSuggested, null);
		tabbedPane.setEnabledAt(3, false);

		JLabel lblThisOptionWill = new JLabel("This Option Will Be Coming Soon!");
		pnlSuggested.add(lblThisOptionWill);

	}

	private void login() {

		try {
			myProfile = new Profile(txtStuID.getText(), new String(txtPassword.getPassword()));
			myProfile.setStudentID(txtStuID.getText());
			char[] pass = txtPassword.getPassword();
			String passString = new String(pass);
			myProfile.setPassword(passString);
		} catch (InvalidParameterException e) {
			JOptionPane.showMessageDialog(frmFitness, "Invalid Password or Username");
			return;
		}

		tabbedPane.setSelectedIndex(1);
		tabbedPane.setEnabledAt(1, true);
		tabbedPane.setEnabledAt(2, true);
		tabbedPane.setEnabledAt(3, true);
		JOptionPane.showMessageDialog(frmFitness, "Welcome " + myProfile.getFirstName());

		setProfileValues();
		// set the student id in the exercise
		ea.setPersonID(Integer.valueOf(myProfile.getStudentID()));
		es.setPersonID(Integer.valueOf(myProfile.getStudentID()));

	}

	private void updateProfile() {

		// check if the values are empty, if they are, do nothing.
		// if they are not, send and update of the value.

		if (!t.isEmpty(txtFirstName.getText())) {
			myProfile.setFirstName(txtFirstName.getText());
		}

		if (!t.isEmpty(txtLastName.getText())) {
			myProfile.setLastName(txtLastName.getText());
		}

		if (!t.isEmpty(txtWeight.getText())) {
			myProfile.setWeightInKilograms(Double.parseDouble(txtWeight.getText()));
		}

		if (!t.isEmpty(txtHeight.getText())) {
			myProfile.setHeightInMeters(Double.parseDouble(txtHeight.getText()));
		}

		if (!t.isEmpty(txtPassUpdate.getText())) {
			myProfile.setPassword(txtPassUpdate.getText());
		}

		if (!t.isEmpty(txtAge.getText())) {

			if (!txtAge.getText().equals("MMDDYYYY")) {
				try {
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");
					myProfile.setBirthday(LocalDate.parse(txtAge.getText(), formatter));
				} catch (DateTimeParseException e) {
					JOptionPane.showMessageDialog(frmFitness, "Invalid Date");
				} catch (NullPointerException n) {
					JOptionPane.showMessageDialog(frmFitness, "Invalid Date");
				}
			}

		}

		if (cboSex.getSelectedItem().toString() == "Select your Sex") {

		} else {
			myProfile.setSex(cboSex.getSelectedItem().toString());
		}

		if ((cboSex.getSelectedIndex() == 0)) {
			myProfile.setSex(String.valueOf(cboSex.getSelectedItem()));
		}

		myProfile.update();

		setProfileValues();

	}

	/**
	 * opens the create account window.
	 * 
	 * @author Michael
	 *
	 */
	private class BtnCreateAccountActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			try {
				NewProfileGui window = new NewProfileGui();
				window.frmNewProfile.setVisible(true);
				frmFitness.setVisible(false);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private class TxtAgeFocusListener extends FocusAdapter {
		@Override
		public void focusGained(FocusEvent e) {
			txtAge.setText("");
		}
	}

	private class TabbedPaneFocusListener extends FocusAdapter {
		@Override
		public void focusGained(FocusEvent arg0) {
			setProfileValues();
			updateExercises();
		}

	}

	private class BtnConvertM2FActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			Conversion convert = new Conversion();
			convert.setPounds(Double.parseDouble(txtConvertToKg.getText()));
			txtWeight.setText(String.valueOf(df.format(convert.getPoundsToKg())));
		}
	}

	private class BtnAddExerciseActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			if (t.isEmpty(txtExerciseDate.getText())) {
				JOptionPane.showInternalMessageDialog(frmFitness, "Please enter a Date for the exercise.");
				return;
			} else {
				if (!txtExerciseDate.getText().equals("MMDDYYYY")) {
					try {
						DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");
						ea.setExerciseDate(LocalDate.parse(txtExerciseDate.getText(), formatter));
						es.setExerciseDate(LocalDate.parse(txtExerciseDate.getText(), formatter));
					} catch (DateTimeParseException e) {
						JOptionPane.showMessageDialog(frmFitness, "Invalid Date");
					} catch (NullPointerException n) {
						JOptionPane.showMessageDialog(frmFitness, "Invalid Date");
					}
				}
			}

			if (t.isEmpty(txtExerciseName.getText())) {
				JOptionPane.showMessageDialog(frmFitness, "Please add an Exercise Name.");
				return;
			} else {
				ea.setExerciseName(txtExerciseName.getText());
				es.setExerciseName(txtExerciseName.getText());
			}

			if (t.isEmpty(txtExerciseTime.getText())) {
				JOptionPane.showMessageDialog(frmFitness, "Please enter the length of the exercise in seconds.");
				return;
			} else {
				ea.setExerciseSeconds(Integer.valueOf(txtExerciseTime.getText()));
				es.setExerciseSeconds(Integer.valueOf(txtExerciseTime.getText()));
			}

			if (!t.isEmpty(txtMaxHeartrate.getText())) {
				ea.setMaximumHeartRate(Integer.valueOf(txtMaxHeartrate.getText()));
			}

			if (!t.isEmpty(txtAverageHeartrate.getText())) {
				ea.setAverageHeartRate(Integer.valueOf(txtAverageHeartrate.getText()));
			}

			if (!t.isEmpty(txtSets.getText())) {
				es.setSets(Integer.valueOf(txtSets.getText()));
			}

			if (!t.isEmpty(txtReps.getText())) {
				es.setReps(Integer.valueOf(txtReps.getText()));
			}

			if (!t.isEmpty(txtWeightsLifted.getText())) {
				es.setWeightLifted(Integer.valueOf(txtWeightsLifted.getText()));
			}

			if (t.isEmpty(txtMaxHeartrate.getText())) {
				if (t.isEmpty(txtAverageHeartrate.getText())) {
					if (t.isEmpty(txtSets.getText())) {
						if (t.isEmpty(txtReps.getText())) {
							if (t.isEmpty(txtWeightsLifted.getText())) {
								JOptionPane.showMessageDialog(frmFitness, "Please do not leave all spaces empty");
								return;
							} // check to see if the user left it all out
						} // seriously I made this method
					} // its not as efficient as it could be
				} // potato
			} // end this mess of if statements

			// if both values are full, send the save aerobics method
			if (!t.isEmpty(txtMaxHeartrate.getText())) {
				if (!t.isEmpty(txtAverageHeartrate.getText())) {
					ea.save();
					myProfile.addAerobic(ea);
				} else {
					JOptionPane.showMessageDialog(frmFitness,
							"Please fill in both values to update an aerobics exercise.");
					return;
				}
			}

			// if all three values are full send the save exercise method
			if (!t.isEmpty(txtSets.getText())) {
				if (!t.isEmpty(txtReps.getText())) {
					if (!t.isEmpty(txtWeightsLifted.getText())) {
						es.save();
						myProfile.addStrength(es);
					} else {
						JOptionPane.showMessageDialog(frmFitness,
								"Make sure all strength fields are " + "filled in before attempting to update.");
						return;
					}
				} else {
					JOptionPane.showMessageDialog(frmFitness,
							"Make sure all strength fields are " + "filled in before attempting to update.");
					return;
				}
			}

			refreshListModelAerobic();
			refreshListModelStength();

		}
	}

	private void setProfileValues() {
		txtCurrentFirstName.setText(myProfile.getFirstName());
		txtCurrentLastName.setText(myProfile.getLastName());
		txtCurrentWeight.setText(String.valueOf(df.format(myProfile.getWeightInKilograms()) + " Kg."));
		txtCurrentHeight.setText(String.valueOf(df.format(myProfile.getHeightInMeters()) + " Meters."));
		txtCurrentAge.setText(String.valueOf(myProfile.getAge() + " years"));
		txtCurrentSex.setText(myProfile.getSex());

	}

	private void updateExercises() {
		refreshListModelAerobic();
		refreshListModelStength();
	}

	// .addKeyListener(new KeyStopperLettersOnly());
	private class KeyStopperLettersOnly extends KeyAdapter {
		public void keyTyped(KeyEvent e) {
			// if any key from 0 to 9 are pressed, consume them
			// t.lettersOnly(e);
		}
	}

	// .addKeyListener(new KeyStopperNumbersOneDecimal());
	private class KeyStopperNumbersOneDecimal extends KeyAdapter {
		public void keyTyped(KeyEvent e) {
			// if any key from 0 to 9 are pressed, consume them
			// t.numbersOneDecimal(e);
		}
	}

	private class KeyStopperNumbersOnly extends KeyAdapter {
		public void keyTyped(KeyEvent e) {
			// if any key from 0 to 9 are pressed, consume them
			// t.numbersOnly(e);
		}
	}

	private class BtnLoadSelectedActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {

		}
	}

	private class BtnUpdate_1ActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			updateExercises();
		}
	}

	public void refreshListModelAerobic() {
		DefaultListModel<LocalDate> dm = new DefaultListModel<>();
		for (ExerciseAerobic e : myProfile.getAerobics()) {
			dm.addElement(e.getExerciseDate());
		}
		listSummary.setModel(dm);
	}

	public void refreshListModelStength() {
		DefaultListModel<LocalDate> dm2 = new DefaultListModel<>();
		for (ExerciseStrength e : myProfile.getStrength()) {
			dm2.addElement(e.getExerciseDate());
		}
		listSummary.setModel(dm2);
	}
}
