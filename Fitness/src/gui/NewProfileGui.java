package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import model.Profile;
import model.Tools;

public class NewProfileGui {

	JFrame frmNewProfile;
	private JPanel pnlCreateAccount;
	private JLabel lblFirstName;
	private JTextField txtFirstName;
	private JLabel lblLastName;
	private JTextField txtLastName;
	private JLabel lblBirthday;
	private JTextField txtHeight;
	private JLabel lblWeight;
	private JTextField txtBirthday;
	private JLabel lblHeight;
	private JTextField txtWeight;
	private JButton btnCreateAccount;
	private JLabel lblPassword;
	private JTextField txtPass;
	private JLabel lblStudentid;
	private JTextField txtStuID;
	private JLabel lblSex;
	private String[] SEX = { "Select your Sex", "Male", "Female", "Other" };
	private JComboBox<String> cboSex;

	Profile myProfile = new Profile();
	Tools t = new Tools();
	Main m = new Main();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewProfileGui window = new NewProfileGui();
					window.frmNewProfile.setVisible(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public NewProfileGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmNewProfile = new JFrame("Create Profile");
		frmNewProfile.getRootPane().setDefaultButton(btnCreateAccount);
		frmNewProfile.setBounds(100, 100, 412, 375);
		frmNewProfile.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frmNewProfile.getContentPane().setLayout(springLayout);

		pnlCreateAccount = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, pnlCreateAccount, 10, SpringLayout.NORTH,
				frmNewProfile.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, pnlCreateAccount, 10, SpringLayout.WEST,
				frmNewProfile.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, pnlCreateAccount, -10, SpringLayout.SOUTH,
				frmNewProfile.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, pnlCreateAccount, 386, SpringLayout.WEST,
				frmNewProfile.getContentPane());
		pnlCreateAccount.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Create New Account",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		frmNewProfile.getContentPane().add(pnlCreateAccount);
		pnlCreateAccount.setLayout(null);

		lblFirstName = new JLabel("First Name");
		lblFirstName.setBounds(10, 82, 130, 14);
		pnlCreateAccount.add(lblFirstName);

		txtFirstName = new JTextField();
		txtFirstName.setBounds(167, 76, 199, 20);
		txtFirstName.setColumns(10);
		pnlCreateAccount.add(txtFirstName);

		lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(10, 107, 122, 14);
		pnlCreateAccount.add(lblLastName);

		txtLastName = new JTextField();
		txtLastName.setBounds(167, 101, 199, 20);
		txtLastName.setColumns(10);
		pnlCreateAccount.add(txtLastName);

		lblBirthday = new JLabel("Birthday \r\n(mmddyyyy)");
		lblBirthday.setBounds(10, 132, 147, 14);
		pnlCreateAccount.add(lblBirthday);

		txtHeight = new JTextField();
		txtHeight.setBounds(167, 176, 199, 20);
		txtHeight.setColumns(10);
		pnlCreateAccount.add(txtHeight);

		lblWeight = new JLabel("Weight (Kg)");
		lblWeight.setBounds(10, 157, 147, 14);
		pnlCreateAccount.add(lblWeight);

		txtBirthday = new JTextField();
		txtBirthday.addFocusListener(new TxtBirthdayFocusListener());
		txtBirthday.setText("MMDDYYYY");
		txtBirthday.setBounds(167, 126, 199, 20);
		txtBirthday.setColumns(10);
		pnlCreateAccount.add(txtBirthday);

		lblHeight = new JLabel("Height (Meters)");
		lblHeight.setBounds(10, 182, 147, 14);
		pnlCreateAccount.add(lblHeight);

		txtWeight = new JTextField();
		txtWeight.setBounds(167, 151, 199, 20);
		txtWeight.setColumns(10);
		pnlCreateAccount.add(txtWeight);

		btnCreateAccount = new JButton("Create Account");
		btnCreateAccount.addActionListener(new BtnCreateAccountActionListener());
		btnCreateAccount.setBounds(124, 247, 130, 38);
		pnlCreateAccount.add(btnCreateAccount);

		lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 57, 130, 14);
		pnlCreateAccount.add(lblPassword);

		txtPass = new JTextField();
		txtPass.setColumns(10);
		txtPass.setBounds(167, 51, 199, 20);
		pnlCreateAccount.add(txtPass);

		lblStudentid = new JLabel("StudentID");
		lblStudentid.setBounds(10, 32, 130, 14);
		pnlCreateAccount.add(lblStudentid);

		txtStuID = new JTextField();
		txtStuID.setColumns(10);
		txtStuID.setBounds(167, 26, 199, 20);
		pnlCreateAccount.add(txtStuID);

		lblSex = new JLabel("Sex");
		lblSex.setBounds(10, 206, 46, 14);
		pnlCreateAccount.add(lblSex);

		cboSex = new JComboBox<String>();
		cboSex.setModel(new DefaultComboBoxModel<>(SEX));
		cboSex.setBounds(167, 203, 199, 20);
		pnlCreateAccount.add(cboSex);
	}

	private class TxtBirthdayFocusListener extends FocusAdapter {
		@Override
		public void focusGained(FocusEvent e) {
			txtBirthday.setText("");
		}
	}

	private class BtnCreateAccountActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {

			if (!t.isEmpty(txtStuID.getText())) {
				myProfile.setStudentID(txtStuID.getText());
			} else {
				JOptionPane.showMessageDialog(frmNewProfile, "Please enter your student ID.");
				return;
			}

			if (!t.isEmpty(txtPass.getText())) {
				myProfile.setPassword(txtPass.getText());
			} else {
				JOptionPane.showMessageDialog(frmNewProfile, "Please enter your Password.");
				return;
			}

			if (!t.isEmpty(txtFirstName.getText())) {
				myProfile.setFirstName(txtFirstName.getText());
			} else {
				JOptionPane.showMessageDialog(frmNewProfile, "Please enter your first name.");
				return;
			}

			if (!t.isEmpty(txtLastName.getText())) {
				myProfile.setLastName(txtLastName.getText());
			} else {
				JOptionPane.showMessageDialog(frmNewProfile, "Please enter your last name.");
				return;
			}

			if (!t.isEmpty(txtWeight.getText())) {
				myProfile.setWeightInKilograms(Double.parseDouble(txtWeight.getText()));
			} else {
				JOptionPane.showMessageDialog(frmNewProfile, "Please enter your weight.");
				return;
			}

			if (!t.isEmpty(txtHeight.getText())) {
				myProfile.setHeightInMeters(Double.parseDouble(txtHeight.getText()));
			} else {
				JOptionPane.showMessageDialog(frmNewProfile, "Please enter your height.");
				return;
			}

			if (cboSex.getSelectedItem().toString() == "Select your Sex") {
				JOptionPane.showMessageDialog(frmNewProfile, "Please select your sex.");
				return;
			}

			myProfile.setSex(cboSex.getSelectedItem().toString());

			try {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");
				myProfile.setBirthday(LocalDate.parse(txtBirthday.getText(), formatter));
			} catch (DateTimeParseException e) {
				JOptionPane.showMessageDialog(frmNewProfile, "Invalid Date");
			} catch (NullPointerException n) {
				JOptionPane.showMessageDialog(frmNewProfile, "Invalid Date");

			}

			// FIXME: duplicate entry
			try {
				myProfile.save();
			} catch (Exception e) {
				if (e instanceof SQLIntegrityConstraintViolationException) {
					JOptionPane.showMessageDialog(frmNewProfile,
							"Profile " + myProfile.getStudentID() + " already exists");
				}

			}

			frmNewProfile.setVisible(false);

			try {
				Main window = new Main();
				window.frmFitness.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}
